# Bienvenu sur mon site web

Il consiste à présenter mon projet PONG, joué par deux cartes STM32F746 liées en série. 

## Description

	Il s'agit d'un jeu d arcade de sport. 
    Il a été imaginé par l'Américain Nolan Bushnell et développé par Allan Alcorn, et la société Atari 
	le commercialise à partir de novembre 1972.
    Le jeu est inspiré du tennis de table en vue de dessus, et chaque joueur s'affronte en déplaçant la 
	raquette virtuelle de haut en bas, via un bouton du PC, de façon à garder la balle dans le terrain 
	de jeu. 
	Un score paramétrable est affiché pour la partie en cours.
	 

## Connaissance utile

* `Commande de la raquette 1` - Touche "a" et "w" du clavier du PC.
* `Commande de la raquette 2` - Touche "8" et "2" de la pavé numérique du PC.
* `Utilisation de la touche tactile` - Configuration du jeu avant le lancement.
* `Liaison série` - Entre 2 cartes STM32F746 via le port UART6 et avec un PC via USB en utilisant un terminal.


